import pygame
from persona_base import PersonajeBase

class Marciano(pygame.sprite.Sprite, PersonajeBase):
    mi_splittesheet = None #Referencia al archivo del spritesheet(".png")
    velocidad= 10 #determina la cantidad de pixeles que se desplaza el spritsheet 
    flames_derecha = {}
    flames_izquierda = {} #Cada uno de los frames de la animación
    frame= 0 #Posición de cadaframe en un momento determinado
    
    def  __init__(self): #Constructor
        self.mi_splittesheet= pygame.image.load("Assets/Spritesheets/marciano.png").convert() #Cargar la imagen en la memoria
        self.frames_izquierda={0:(0,0,30,48), 1:(30,0,30,48), #(x, y , ancho, alto)
                               2:(60,0,30,48), 3:(90,0,30,48)}
        self.frames_derecha={0:(150,0,30,48), 1:(180,0,30,48), 
                             2:(210,0,30,48), 3:(240,0,30,48)}
                             
        self.mi_splittesheet.set_clip(pygame.Rect(0,0,30,48)) #set_clip; método de sprite
        self.image= self.mi_splittesheet.subsurface(self.mi_splittesheet.get_clip()) #subsurface; método de sprite
        self.rect= self.image.get_rect() #image, rect= atributos que hereda de sprite

#Modifica el movimiento del personaje desde el eje x o y.
    def update(self, direccion):

        if direccion == "derecha":
            self.rect.x+=self.velocidad

        if direccion == "izquierda":
            self.rect.x-=self.velocidad
        
        if direccion == "abajo":
            self.rect.y+=self.velocidad

        if direccion== "arriba":
            self.rect.y-=self.velocidad

#Ejecuta el método 'update' si el if es correcto
    def administrar_evento(self,evento):
        if evento.type == pygame.KEYDOWN: #keydown: tipo de evento(presionar una tecla)
            if evento.key == pygame.K_RIGHT: #Verifica qué tecla presionó el usuario
                self.update("derecha")
        
            if evento.key == pygame.K_LEFT:
                self.update("izquierda")

            if evento.key == pygame.K_UP:
                self.update("arriba")

            if evento.key == pygame.K_DOWN:
                self.update("abajo") 
    
    