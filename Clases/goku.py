import pygame
from persona_base import PersonajeBase

class Goku(pygame.sprite.Sprite, PersonajeBase):
    mi_splittesheet = None
    flames_derecha = {}
    flames_izquierda = {}
    frame= 0
    
    def  __init__(self):
        self.mi_splittesheet= pygame.image.load("Assets/Spritesheets/Goku_modificado.png").convert() #Cargar la imagen en la memoria
        self.frames_izquierda={0:(16,14,33,91),1:(13,121,40,80), #(x, y , ancho, alto)
                                2:(58,130,80,68)}
        self.frames_derecha={0:(16,14,33,91), 1:(13,121,40,80), 
                             2:(58,130,80,68)}
        self.mi_splittesheet.set_clip(pygame.Rect(16,14,33,91)) #set_clip; método de sprite
        self.image= self.mi_splittesheet.subsurface(self.mi_splittesheet.get_clip()) #subsurface; método de sprite
        self.rect= self.image.get_rect() #image, rect= atributos que hereda de sprite

#Le da movimiento al personaje, controla el indice de cada frame (En este caso, el máximo es 4 y vuelve a empezar)
#Guarda el frame "final" que le devuelve 'devolver_frame'
        
#Modifica el movimiento del personaje desde el eje x o y.
    def update(self, direccion):
        if direccion == "derecha":
            self.rect.x+=10

        if direccion == "izquierda":
            self.rect.x-=10
        
        if direccion == "abajo":
            self.rect.y+=10

        if direccion== "arriba":
            self.rect.y-=10

#Ejecuta el método 'update' si el if es correcto
    def administrar_evento(self,evento):
        if pygame.key.get_pressed()[pygame.K_d]:
            self.update("derecha")
        elif pygame.key.get_pressed()[pygame.K_a]:
            self.update("izquierda")