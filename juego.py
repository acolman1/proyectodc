from Clases.marciano import Marciano
import pygame
import sys
from Clases.goku import Goku

ancho = 300 #define variable del ancho de la ventana
alto = 500 #define variable del alto de la ventana

class Juego:

    pygame.init() #inicia pygame
    ventana = pygame.display.set_mode((ancho,alto)) #crea la ventana
    pygame.display.set_caption("proyecto")#cambia titulo de la ventana

    reloj = pygame.time.Clock()
    goku = Goku()
    marciano = Marciano()

    def jugar(self):
        while True:
            self.ventana.fill([10,200,18])#cambia el color del fondo
            for evento in pygame.event.get():
                if (evento.type == pygame.QUIT):
                    sys.exit()
            if self.marciano.rect.x > 300:
                self.marciano.rect.x=0
                    
            if self.marciano.rect.x < 0:
                self.marciano.rect.x=300   

            if self.marciano.rect.y < 0:
                self,marciano.rect.y=500   
            
            if self.marciano.rect.y > 500:
                self.marciano.rect.y=0   

            if self.marciano.rect.y < 400:
                self.marciano.rect.y +=10

            self.ventana.blit(self.goku.image, self.goku.rect)
            self.goku.administrar_evento(evento)
            self.ventana.blit(self.marciano.image, self.marciano.rect)
            self.marciano.administrar_evento(evento)
            self.reloj.tick(30) #FPS. El reloj le da velocidad a los frames
            pygame.display.update()#actualiza la ventana
        
iniciar= Juego()
iniciar.jugar()
