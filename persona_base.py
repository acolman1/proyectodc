class PersonajeBase:
#Le da movimiento al personaje, controla el indice de cada frame (En este caso, el máximo es 4 y vuelve a empezar)
    def devolver_frame(self, animacion):
        self.frame+=1
        if self.frame>=len(animacion):
            self.frame=0
        return animacion[self.frame]

#Guarda el frame "final" que le devuelve 'devolver_frame'
    def clip(self, animacion):
        self.mi_splittesheet.set_clip(pygame.Rect(self.devolver_frame(animacion)))